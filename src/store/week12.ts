import { createStore } from 'vuex'

export default createStore({
  state: {
    games: {
      1: { away: 'HOU', home: 'DET' },
      2: { away: 'WSH', home: 'DAL' },
      3: { away: 'LV', home: 'ATL' },
      4: { away: 'LAC', home: 'BUF' },
      5: { away: 'NYG', home: 'CIN' },
      6: { away: 'TEN', home: 'IND' },
      7: { away: 'CAR', home: 'MIN' },
      8: { away: 'ARI', home: 'NE' },
      9: { away: 'MIA', home: 'NYJ' },
      10: { away: 'CLE', home: 'JAX' },
      11: { away: 'BAL', home: 'PIT' },
      12: { away: 'NO', home: 'DEN' },
      13: { away: 'SF', home: 'LAR' },
      14: { away: 'KC', home: 'TB' },
      15: { away: 'CHI', home: 'GB' },
      16: { away: 'SEA', home: 'PHI' }
    },
    picks: {
      Casey: {
        1: 'away',
        2: 'away',
        3: 'away',
        4: 'home',
        5: 'away',
        6: 'away',
        7: 'home',
        8: 'away',
        9: 'away',
        10: 'home',
        11: 'home',
        12: 'home',
        13: 'home',
        14: 'away',
        15: 'away',
        16: 'away',
        tiebreaker: 52
      },
      Don: {
        1: 'home',
        2: 'home',
        3: 'away',
        4: 'home',
        5: 'away',
        6: 'away',
        7: 'away',
        8: 'away',
        9: 'away',
        10: 'away',
        11: 'home',
        12: 'away',
        13: 'home',
        14: 'away',
        15: 'away',
        16: 'away',
        tiebreaker: 32
      },
      John: {
        1: 'home',
        2: 'away',
        3: 'home',
        4: 'away',
        5: 'home',
        6: 'home',
        7: 'away',
        8: 'away',
        9: 'away',
        10: 'home',
        11: 'home',
        12: 'away',
        13: 'home',
        14: 'away',
        15: 'away',
        16: 'home',
        tiebreaker: 8
      },
      Lee: {
        1: 'away',
        2: 'home',
        3: 'away',
        4: 'home',
        5: 'home',
        6: 'away',
        7: 'home',
        8: 'away',
        9: 'away',
        10: 'away',
        11: 'home',
        12: 'away',
        13: 'home',
        14: 'away',
        15: 'home',
        16: 'away',
        tiebreaker: 36
      },
      Megan: {
        1: 'home',
        2: 'home',
        3: 'away',
        4: 'home',
        5: 'away',
        6: 'home',
        7: 'home',
        8: 'home',
        9: 'away',
        10: 'away',
        11: 'away',
        12: 'away',
        13: 'away',
        14: 'away',
        15: 'away',
        16: 'away',
        tiebreaker: 56
      },
      Stacey: {
        1: 'home',
        2: 'away',
        3: 'away',
        4: 'home',
        5: 'away',
        6: 'away',
        7: 'away',
        8: 'home',
        9: 'away',
        10: 'away',
        11: 'home',
        12: 'home',
        13: 'home',
        14: 'away',
        15: 'away',
        16: 'home',
        tiebreaker: 31
      }
    },
    winners: {
      1: 'away',
      2: 'away',
      3: 'home',
      4: 'home',
      5: 'away',
      6: 'away',
      7: 'home',
      8: 'home',
      9: 'away',
      10: 'away',
      11: 'home',
      12: 'away',
      13: 'away',
      14: 'away',
      15: 'home',
      16: 'away',
      tiebreaker: 40
    }
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
